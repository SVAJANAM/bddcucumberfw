package stepDefinitions;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import EnvironmentandRepository.Environment;
import EnvironmentandRepository.Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_STepdef {
	 String requestbody;
	    String endpoint;
	    Response response;
	    ResponseBody responseBody;
	    int statuscode;

	    @Given("Retrieve the details of delete API with payload")
	    public void retrieve_the_details_of_delete_API_with_payload() {
	        requestbody = Repository.delete_request_body();
	        endpoint = Environment.delete_endPoint();
	    }

	    @Given("Retrieve the details of delete API without payload")
	    public void retrieve_the_details_of_delete_API_without_payload() {
	        requestbody = "";
	        endpoint = Environment.delete_endPoint();
	    }

	    @When("Send the delete request")
	    public void send_the_delete_request() {
	        response = API_Trigger.delete_API_Trigger(requestbody, endpoint);
	    }

	    @Then("Validate status code for delete API")
	    public void validate_status_code_for_delete_API() {
	        statuscode = response.statusCode();
	        Assert.assertEquals(statuscode, 201, "Status code is not equal to 204");
	    }

	    @Then("Validate response body parameters for delete API")
	    public void validate_response_body_parameters_for_delete_API() {
	        responseBody = response.getBody();
	        System.out.println(responseBody.asString());

	        Response response= API_Trigger.delete_API_Trigger(requestbody, endpoint);
	    	int statuscode=response.statusCode();
	    	System.out.println(statuscode);
	    }
}