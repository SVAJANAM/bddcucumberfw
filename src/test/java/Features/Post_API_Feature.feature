Feature: Trigger the POST API with required parameters 

@Post_API
Scenario: Trigger the API request with valid request body parameters 
	Given Enter NAME and JOB in request body 
	When Send the request with payload 
	Then Validate status code 
	And Validate response body parameters 

@Post_API	
Scenario Outline: Test post api with multiple data set 
	Given  Enter "<NAME>" and "<JOB>" in request body
	When  Send the request with payload 
	Then  Validate status code 
	And Validate response body parameters 
	
Examples: 
	|NAME|JOB|
	|Atharv|TESTER|
	|Supriya|Manager|
	|Ashmika|Developer|
	|Akash|senior manager|
		
     