package CommonMethods;

	import EnvironmentandRepository.Repository;
import EnvironmentandRepository.RequestRepository;
	import io.restassured.RestAssured;
	import io.restassured.response.Response;
	import io.restassured.specification.RequestSpecification;

	public class API_Trigger extends Repository {

		static String headername = "Content-Type";
		static String headervalue = "application/json";

		public static Response Post_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.post(endPoint);
			return response;
		}
		
		public static Response Put_API_Trigger(String requestBody, String endPoint) {
			
			RequestSpecification req_spec = RestAssured.given();	
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response resp = req_spec.put(endPoint);
			return resp;
	}


	public static Response Patch_API_Trigger(String requestBody, String endPoint) {
		
		RequestSpecification req_spec = RestAssured.given();	
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response resp = req_spec.patch(endPoint);
		return resp;
	}

	public static Response get_API_Trigger(String requestBody, String endPoint) {
		
		RequestSpecification req_spec = RestAssured.given();	
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response resp = req_spec.get(endPoint);
		return resp;
	}

	public static Response delete_API_Trigger(String requestBody, String endPoint) {
		
		RequestSpecification req_spec = RestAssured.given();	
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response resp = req_spec.post(endPoint);
		return resp;
	}
	}
	