Feature: Trigger the Put API with required parameters 

@Put_API
Scenario: Trigger the Put API request with valid request body parameters 
	Given Enter NAME and JOB in request Body 
	When Send the request with Payload
	Then Validate status Code
	And Validate response body Parameters

@Put_API	
Scenario Outline: Test put api with multiple data set 
	Given  Enter "<NAME>" and "<JOB>" in request Body
	When  Send the request with Payload
	Then  Validate status Code
	And Validate response body Parameters
	
Examples: 
	|NAME|JOB|
	|Radhika|Test lead|
	|Manali|Consultant|
	
		