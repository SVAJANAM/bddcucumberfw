package stepDefinitions;

import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import EnvironmentandRepository.Environment;
import EnvironmentandRepository.Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;

public class Get_API_Stepdef {
    
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	
	int exp_page = 2;
	int exp_per_page = 6;
	int exp_total = 12;
	int id[] = { 7, 8, 9, 10, 11, 12 };
	String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
	String first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
	String last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
	String avatar[] = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
			"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
			"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };
	String exp_url = "https://reqres.in/#support-heading";
	int exp_total_page = 2;

	@Given("Retrive the details of Get API")
	public void Retrive_the_details_of_Get_API() {
		requestbody = Repository.get_request_body();
		endpoint = Environment.get_endPoint();

	}

	@Given("Retrive  the details of Get API")
	public void Retrive_the_details_of_Get_API1() {
		requestbody = "";
		endpoint = Environment.get_endPoint();

	}

	@When("Send The Request with payload")
	public void Send_The_Request_with_payload() {
		response = API_Trigger.get_API_Trigger(requestbody, endpoint);

	}

	@Then("validate Status code")
	public void validate_Status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "statuscode is not equal to statuscode sent in Request Body");

	}

	@Then("validate Response body parameters")
	public void validate_Response_body_parameters() {
		responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

	
		Assert.assertEquals(exp_page, exp_page);
		Assert.assertEquals(exp_per_page, exp_per_page);
		Assert.assertEquals(exp_total, exp_total);
		Assert.assertEquals(exp_total_page, exp_total_page);

		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), email[i],
					"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i],
					"Validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i],
					"Validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i],
					"Validation of avatar failed for json object at index : " + i);
		}

	}
}