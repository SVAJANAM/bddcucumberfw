package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvironmentandRepository.Environment;
import EnvironmentandRepository.Repository;
import EnvironmentandRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_API_StepDefi {
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Enter NAME and JOB in request body")
	public void enter_name_and_job_in_request_body() {
		requestbody = Repository.post_request_body();
		endpoint = Environment.post_endpoint();

	}

	@Given("Enter {string} and {string} in request body")
	public void enter_and_in_request_body(String string, String string2) {
		requestbody = "{\r\n" + "    \"name\": \"" + string + "\",\r\n" + "    \"job\": \"" + string2 + "\"\r\n" + "}";
		endpoint = Environment.post_endpoint();

	}

	@When("Send the request with payload")
	public void send_the_request_with_payload() {
		response = API_Trigger.Post_API_Trigger(requestbody, endpoint);

	}

	@Then("Validate status code")
	public void validate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 201, "statuscode is not equal to statuscode sent in Request Body");

	}

	@Then("Validate response body parameters")
	public void validate_response_body_parameters() {
		responseBody = response.getBody();
		System.out.println(responseBody.asString());

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

}
