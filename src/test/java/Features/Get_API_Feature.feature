Feature: Trigger the Get API with required parameters 

@Get_API
Scenario: Trigger the API request with valid request body parameters 
	Given Retrive the details of Get API
	When Send The Request with payload 
	Then validate Status code 
	And validate Response body parameters 

@Get_API	
Scenario Outline: Test Get api with multiple data set 
	Given  Retrive the details of Get API
	When  Send The Request with payload 
	Then  validate Status code 
	And validate Response body parameters 
	
Examples: 
	|Id|email|first_name|last_name|avatar|
	|13|"Supriya.Vajanam@reqres.in"|"Supriya"|"Vajanam"|"https://reqres.in/img/faces/8-image.jpg"|
	|14|"Atharv.Pandey@reqres.in"|"Atharv"|"Pandey"|"https://reqres.in/img/faces/9-image.jpg"|
	