package EnvironmentandRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethods.Utilities;

public class Repository extends Environment {

	public static String post_param_requestBody(String testcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExceldata("Post_API", testcaseName);
		String Key1 = data.get(1);
		String req_name = data.get(2);
		String Key2 = data.get(3);
		String req_job = data.get(4);
		String requestBody = "{\r\n" + "    \""+Key1+"\": \""+req_name+"\",\r\n" + "    \""+Key2+"\": \""+req_job+"\"\r\n" + "}";

		return requestBody;

	}

	public static String post_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		return requestBody;
	}

	public static String put_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestBody;
	}

	public static String patch_request_body() {
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestbody;
	}

	public static String get_request_body() {
		String requestbody = "";
		return requestbody;
	}

	public static String delete_request_body() {
		String requestbody = "";
		return requestbody;
	}

}