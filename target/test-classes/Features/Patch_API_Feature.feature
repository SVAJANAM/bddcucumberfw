Feature: Trigger the Patch API with required parameters 

@Patch_API
Scenario: Trigger the API request with valid request body parameters 
	Given Enter NAME and JOB in Request body 
	When Send the Request with payload 
	Then Validate Status code 
	And Validate Response body parameters 

@Patch_API	
Scenario Outline: Test patch api with multiple data set 
	Given  Enter "<NAME>" and "<JOB>" in Request body
	When  Send the Request with payload 
	Then  Validate Status code 
	And Validate Response body parameters 
	
Examples: 
	|NAME|JOB|
	|Pratibha|Senior Manager|
	|Suresh|Director|
	