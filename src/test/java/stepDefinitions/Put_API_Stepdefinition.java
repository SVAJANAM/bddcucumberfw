package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvironmentandRepository.Environment;
import EnvironmentandRepository.Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_API_Stepdefinition {

	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

@Given("Enter NAME and JOB in request Body")
public void enter_name_and_job_in_request_Body() {
	requestbody = Repository.put_request_body();
	endpoint = Environment.put_endPoint();

}

@Given("Enter {string} and {string} in request Body")
public void enter_and_in_request_Body(String string, String string2) {
	requestbody = "{\r\n" + "    \"name\": \""+string+"\",\r\n" + "    \"job\": \""+string2+"\"\r\n" + "}";
	endpoint = Environment.put_endPoint();

}

@When("Send the request with Payload")
public void send_the_request_with_Payload() {
	response = API_Trigger.Put_API_Trigger(requestbody, endpoint);

}

@Then("Validate status Code")
public void validate_status_Code() {
	int statuscode = response.statusCode();
	Assert.assertEquals(statuscode, 200, "statuscode is not equal to statuscode sent in Request Body");

}
@Then("Validate response body Parameters")
public void validate_response_body_Parameters() {
	responseBody = response.getBody();
	System.out.println(responseBody.asString());
	
	
	
	String res_name = responseBody.jsonPath().getString("name");
	String res_job = responseBody.jsonPath().getString("job");
	//String res_id = responseBody.jsonPath().getString("id");
	String res_updatedAt=responseBody.jsonPath().getString("updatedAt");
	res_updatedAt=res_updatedAt.toString().substring(0,11);


	JsonPath jsp_req = new JsonPath(requestbody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");

	LocalDateTime currentdate = LocalDateTime.now();
	String expecteddate = currentdate.toString().substring(0, 11);
    
	
	Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
	Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
	//Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
	Assert.assertEquals(res_updatedAt,expecteddate," creaed at in response body is not eual to date gengerted");

}
}
